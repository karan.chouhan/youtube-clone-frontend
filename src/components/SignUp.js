import React, { useState } from 'react'
import './SignUp.css'
import axios from 'axios'

function SignUp({ history }) {

    const [name, setname] = useState("");
    const [username, setusername] = useState("")
    const [email, setemail] = useState("")
    const [password, setpassword] = useState("")
    const [isEmpty, setisEmpty] = useState(false)

    const saveName = (data) => {
        setname(data.target.value)
    }
    const saveUsername = (data) => {
        setusername(data.target.value);
    }
    const saveEmail = (data) => {
        setemail(data.target.value);
    }
    const savePassword = (data) => {
        setpassword(data.target.value)
    }

    const submit = () => {
        if (name === "" || username === "" || email === "" || password === "") {
            setisEmpty(true)
            return;
        } else {
            setisEmpty(false)
            console.log(name);
            console.log(username)
            console.log(email);
            console.log(password)

            axios.post('http://localhost:8080/api/auth/signup', {
                "name": name,
                "username": username,
                "email": email,
                "password": password
            })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });

            // history.push('/')
        }

    }


    return (
        <div className="signup-div">
            <div className="form-div">
                <h2>Fill in the following details</h2>
                <input
                    className="signup-input"
                    placeholder="YOUR NAME"
                    onChange={e => saveName(e)}
                />
                <input
                    className="signup-input"
                    placeholder="GIVE YOURSELF A COOL USER NAME"
                    onChange={e => saveUsername(e)}
                />
                <input
                    className="signup-input"
                    placeholder="YOUR EMAIL"
                    onChange={e => saveEmail(e)}
                />
                <input
                    className="signup-input"
                    type="password"
                    placeholder="PASSWORD"
                    onChange={e => savePassword(e)}
                />
                <button className="signup-btn" onClick={submit} >REGISTER</button>
                <p className={isEmpty ? "error" : "hide"} >please provide all details</p>
            </div>
        </div>
    )
}

export default SignUp
