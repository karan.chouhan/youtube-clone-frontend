import React from 'react'
import HomeIcon from '@material-ui/icons/Home'
import SubscriptionsIcon from '@material-ui/icons/Subscriptions'
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo'
import WatchLaterIcon from '@material-ui/icons/WatchLater'
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined'
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';
import SidebarRow from './SidebarRow'
import './Sidebar.css'

export default function Sidebar() {
    return (
        <div className="sidebar">
            <SidebarRow selected Icon={HomeIcon} title="Home" />
            <SidebarRow Icon={SubscriptionsIcon} title="Subscription" />
            <hr />
            <SidebarRow Icon={OndemandVideoIcon} title="Your Videos" />
            <SidebarRow Icon={WatchLaterIcon} title="Watch Later" />
            <SidebarRow Icon={ThumbUpAltOutlinedIcon} title="Liked Videos" />
            <hr />
            <SidebarRow Icon={PlaylistPlayIcon} title="Playlist 1" />
            <SidebarRow Icon={PlaylistPlayIcon} title="Playlist 2" />
        </div>
    )
}