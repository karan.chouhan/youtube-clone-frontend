import React, { useState, useEffect } from 'react'
import './signIn.css';
import { Link } from 'react-router-dom'

function signIn({ history }) {
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("")


    const saveName = (name) => {
        name.preventDefault();
        setUserName(name.target.value);
    }

    const savePass = (password) => {
        setPassword(password.target.value)
    }

    const submit = () => {
        console.log(userName);
        console.log(password);
        history.push("/")
    }
    return (
        <div className="sign-in">
            <div className="logo-div">
                <img className="logo" src="https://lh3.googleusercontent.com/3zkP2SYe7yYoKKe47bsNe44yTgb4Ukh__rBbwXwgkjNRe4PykGG409ozBxzxkrubV7zHKjfxq6y9ShogWtMBMPyB3jiNps91LoNH8A=s500"></img>
            </div>
            <div className="login-form">
                <input className="login-input" placeholder="USER NAME" onChange={e => saveName(e)} />
                <input className="login-input" type="password" placeholder="PASSWORD" onChange={e => savePass(e)} />
                <button className="login-btn" onClick={submit} >SIGN IN</button>
                <Link to='/sign-up' className="login-link">Visiting for first time! click here to register</Link>
            </div>
        </div>
    )
}

export default signIn
