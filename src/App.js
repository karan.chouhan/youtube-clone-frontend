import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import SignIn from './components/signIn';
import SignUp from './components/SignUp'
import Home from './components/Home'
import Sidebar from './components/Sidebar'
import Header from './components/Header'

function App() {
  let x = false
  let y = "k"
  if (x) {
    y =
      <div>
        {/* <Redirect to='/sign-in' /> */}
        <Route path='/sign-in' exact component={SignIn} />
        <Route path='/sign-up' exact component={SignUp} />
      </div>
  } else {
    y = <div>
      <Header />
      <div className="main">
        <Sidebar />
        <Route path='/' exact component={Home} />
      </div>
    </div>
  }
  return (
    <Router>
      <div>
        <Switch>
          <Route path='/' exact>
            <Header />
            <div className="main">
              <Sidebar />
              <Home />
            </div>
          </Route>
          <Route path='/sign-in' exact exact component={SignIn} />
          <Route path='/sign-up' exact component={SignUp} />
        </Switch>
      </div>
    </Router>
  )
}

export default App

